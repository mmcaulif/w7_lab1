//sensor variables
const int RF = 5;
const int RB = 4;
const int LF = 6;
const int LB = 7;
const int REYE = A3;
const int LEYE = A4;
const int ECHO = 8;
const int TRIG = 9;
const int WHEEL = 2;

//speedometer variables
bool temporaryVAL;
int wheelcount;
double wheelRadius = 0.032;
double sensor1EventTime, sensor2EventTime;
double recordedTime;
double angularVelocity, velocity;

//misc. variables
bool inputBool, driveLoop; 
int UScount;
int REPcount;
int following_case;
double jdistance;

//xbee variables
bool started = false;
bool ended = false;
char incomingByte; 
char msg[3];
byte index;

void setup() {
  Serial.begin(9600);
  delay(1000);
  Serial.println("+++");
  delay(1000);
  Serial.println("ATID 3007, CH C, CN");
  delay(1000);
  while( Serial.read() != -1 );
  
  pinMode(LEYE,INPUT);
  pinMode(REYE,INPUT);
  pinMode(ECHO,INPUT);
  pinMode(WHEEL,INPUT);
  pinMode(TRIG,OUTPUT);
  pinMode(RF,OUTPUT);
  pinMode(RB,OUTPUT);
  pinMode(LF,OUTPUT);
  pinMode(LB,OUTPUT);
  wheelcount = 0;
  temporaryVAL = 0;
  sensor2EventTime = 0;
  jdistance = 0;
  
  //xbee testing
  Serial.println("Buggy says hello");
  Serial.println("I am waiting to be told to go");
  driveLoop = false;
}
void loop() {
//speedometer
 bool VAL = digitalRead(WHEEL);
  if (VAL != temporaryVAL) {
    temporaryVAL = VAL;
    wheelcount = wheelcount + 1;
    if (wheelcount%20 == 0) {
      sensor1EventTime = millis();
      recordedTime = (sensor1EventTime - sensor2EventTime)/1000;
      angularVelocity = 4*PI/recordedTime;
      velocity = wheelRadius*angularVelocity;
      jdistance = jdistance + 4*PI*wheelRadius;
      sensor2EventTime = sensor1EventTime;
    }
  }
//ultrasonics
  bool US = false;
  int distance;
  long duration;
  if (UScount > 4800){
      digitalWrite(TRIG, LOW);
      delayMicroseconds(2);
       
      digitalWrite(TRIG, HIGH);
      delayMicroseconds(10);
      digitalWrite(TRIG, LOW);
         
      duration = pulseIn(ECHO, HIGH);
      distance = duration/58;

      if(distance < 20){
        US = true;
        UScount = 4801;
        jdistance = 0;
      }
      else{
        US = false;
        UScount = 0;
      }
    
    
    }
    

//visual sensors
  bool RVAL = digitalRead(REYE);
  bool LVAL = digitalRead(LEYE);
  
//cases
    if(US == false){
      following_case = 8*(!driveLoop) + 4*(US) + 2*(!LVAL) + (!RVAL);
    }
    else if (US == true){
      following_case = 4;
    }
    //Serial.print(following_case);
    switch(following_case){
      case 0:
        analogWrite(RF,160);
        analogWrite(LF,160);
        //Serial.println("im going straight");
        break;
    case 1:
        analogWrite(RF,200);
        analogWrite(LF,10);
        //Serial.println("im going right");
        break;
    case 2:
        analogWrite(LF,200);
        analogWrite(RF,10);
        //Serial.println("im going left");
      break;
    case 3: //both white
        analogWrite(RF,140);
        analogWrite(LF,70);
        //Serial.println("junction");
      break;
    case 4:
        analogWrite(RF,0);
        analogWrite(LF,0);
        //Serial.println("Something's in the way! 1");
      break;
    case 5:
        analogWrite(RF,0);
        analogWrite(LF,0);
        //Serial.println("Something's in the way! 2");
      break;
    case 6:
        analogWrite(RF,0);
        analogWrite(LF,0);
        //Serial.println("Something's in the way! 3");
      break;
    case 7:
        analogWrite(RF,0);
        analogWrite(LF,0);
        //Serial.println("Something's in the way! 4");
      break;
    case 8:
        analogWrite(RF,0);
        analogWrite(LF,0);
        //Serial.println("I've been told to stop");
      break;
    case 9:
        analogWrite(RF,0);
        analogWrite(LF,0);
        //Serial.println("I've been told to stop");
      break;
    case 10:
        analogWrite(RF,0);
        analogWrite(LF,0);
        //Serial.println("I've been told to stop");
      break;
    case 11:
        analogWrite(RF,0);
        analogWrite(LF,0);
        //Serial.println("I've been told to stop");
      break;
    case 12:
        analogWrite(RF,0);
        analogWrite(LF,0);
        //Serial.println("I've been told to stop");
      break;
    case 13:
        analogWrite(RF,0);
        analogWrite(LF,0);
        //Serial.println("I've been told to stop");
      break;
    case 14:
        analogWrite(RF,0);
        analogWrite(LF,0);
        //Serial.println("I've been told to stop");
      break;
    case 15:
        analogWrite(RF,0);
        analogWrite(LF,0);
        //Serial.println("I've been told to stop");
      break;
    default :
        Serial.print("impossible \n");
       break;
    }
  UScount = UScount + 1;
  REPcount = REPcount + 1;

  //report  
  if(REPcount > 9600) {
    Serial.print("REPORT: ");
    if (driveLoop == true){
      if (US == false) {
        Serial.print("My speed is: ");
        Serial.print(velocity);
        Serial.print(" and I have travelled ");
        Serial.print(jdistance);
        Serial.print("m since I last stopped.\n");
      }
      else if (US == true) {
        Serial.print("There is something in my way\n");
      }
    }
    else if (driveLoop == false) {
      Serial.print("You have told me to stop\n");
    }
    
    REPcount = 0;
  }
}


void serialEvent() {

  while (Serial.available()>0) {
    incomingByte = Serial.read();
    if(incomingByte == '<') {
     started = true;
     index = 0;
     msg[index] = '\0';
    }
    
    else if(incomingByte == '>') {
     ended = true;
     break;
    }
    
    else {
     if(index < 4) {
       msg[index] = incomingByte;
       index++;
       msg[index] = '\0'; 
     }
   }
 }
 
 if(started && ended) {
   //handling xbee comms
   if (msg[0] == 's') {
     driveLoop = false;
     Serial.println("Stopping");
   }
   else if (msg[0] == 'g') {
     driveLoop = true;
     Serial.println("Going");
     jdistance = 0;
   }
   else{
    Serial.println("Invalid command, try again");
   }
   
   index = 0;
   msg[index] = '\0';
   started = false;
   ended = false;

 }

}
