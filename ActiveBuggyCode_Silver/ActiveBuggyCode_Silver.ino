#include <SPI.h>
#include <Pixy.h>

//pixycam variables
Pixy pixy;

//sensor variables
const int RF = 5;
const int RB = 4;
const int LF = 6;
const int LB = 7;
const int REYE = A3;
const int LEYE = A4;
const int ECHO = 8;
const int TRIG = 9;
const int WHEEL = 2;

//speedometer variables
bool temporaryVAL;
int wheelcount;
double wheelRadius = 0.032;
double sensor1EventTime, sensor2EventTime;
double recordedTime;
double angularVelocity, velocity;

//misc. variables
bool inputBool, driveLoop; 
int UScount;
int REPcount;
int following_case;
double jdistance;

//xbee variables
bool started = false;
bool ended = false;
char incomingByte; 
char msg[3];
byte index;

void setup() {
  Serial.begin(9600);
  delay(1000);
  Serial.println("+++");
  delay(1000);
  Serial.println("ATID 3007, CH C, CN");
  delay(1000);
  while( Serial.read() != -1 );
  
  pinMode(LEYE,INPUT);
  pinMode(REYE,INPUT);
  pinMode(ECHO,INPUT);
  pinMode(WHEEL,INPUT);
  pinMode(TRIG,OUTPUT);
  pinMode(RF,OUTPUT);
  pinMode(RB,OUTPUT);
  pinMode(LF,OUTPUT);
  pinMode(LB,OUTPUT);
  wheelcount = 0;
  temporaryVAL = 0;
  sensor2EventTime = 0;
  jdistance = 0;
  
  //xbee testing
  Serial.println("Buggy says hello");
  Serial.println("Searching for blocks");
  driveLoop = false;

  //pixy setup
  pixy.init();
}

void loop() {

//pixycam
  uint16_t blocks;
  blocks = pixy.getBlocks();
  Serial.println(blocks);
  
  if (blocks > 0){
    Serial.print("I detect: ");
    Serial.print(blocks);
    Serial.print(" blocks");
    for(int i = 0; i<blocks; i++){
      Serial.print(" block ");
      Serial.print(i);
      Serial.print(": ");
      pixy.blocks[i].print();
      Serial.print("\n");
    }
  }
  delay(1000);
}


void serialEvent() {

  while (Serial.available()>0) {
    incomingByte = Serial.read();
    if(incomingByte == '<') {
     started = true;
     index = 0;
     msg[index] = '\0';
    }
    
    else if(incomingByte == '>') {
     ended = true;
     break;
    }
    
    else {
     if(index < 4) {
       msg[index] = incomingByte;
       index++;
       msg[index] = '\0'; 
     }
   }
 }
 
 if(started && ended) {
   //handling xbee comms
   if (msg[0] == 's') {
     driveLoop = false;
     Serial.println("Stopping");
   }
   else if (msg[0] == 'g') {
     driveLoop = true;
     Serial.println("Going");
     jdistance = 0;
   }
   else{
    Serial.println("Invalid command, try again");
   }
   
   index = 0;
   msg[index] = '\0';
   started = false;
   ended = false;

 }

}
